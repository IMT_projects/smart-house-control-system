#ifndef CUSTOM_TYPES_H_
#define CUSTOM_TYPES_H_


// signed integers
typedef signed char          s8;
typedef signed short int     s16;
typedef signed long int      s32;
typedef signed long long int s64;

// unsigned integers
typedef unsigned char          u8 ;
typedef unsigned short int     u16;
typedef unsigned long int      u32;
typedef unsigned long long int u64;

// float (always signed)
typedef float  f32;
typedef double f64;


#endif /* CUSTOM_TYPES_H_ */

