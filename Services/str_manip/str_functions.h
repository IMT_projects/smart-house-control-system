#ifndef UTILITIES_UTILITIES_FUNCS_H_
#define UTILITIES_UTILITIES_FUNCS_H_


#include "../../custom_types.h"

u8 str_u8StrLength(const char* charPtrStrCpy);

u8 str_u8StrCmp(const char* charPtrStr_1Cpy, const char* charPtrStr_2Cpy);

void str_vidStrCopy(char* charPtrDestCpy, u8 u8LengthCpy, const char* charPtrSrcCpy);


#endif /* UTILITIES_UTILITIES_FUNCS_H_ */

