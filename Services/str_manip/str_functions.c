#include "../str_manip/str_functions.h"


inline u8 str_u8StrLength(const char* charPtrStrCpy)
{
	u8 i = 0;
	while (charPtrStrCpy[i++]); // count after null (length until null)

	return (i - 1); // don't count null
}

u8 str_u8StrCmp(const char* charPtrStr_1Cpy, const char* charPtrStr_2Cpy)
{
	u8 i = 0;

	for (; charPtrStr_1Cpy[i] && charPtrStr_2Cpy[i]; i++)
	{
		if (charPtrStr_1Cpy[i] != charPtrStr_2Cpy[i])
			return 0;
	}

    // BUGFIX: solves the problem where str1 is a sub-string of str2
	return (charPtrStr_1Cpy[i] == charPtrStr_2Cpy[i]);
}

void str_vidStrCopy(char* charPtrDestCpy, u8 u8LengthCpy, const char* charPtrSrcCpy)
{
	u8 i = 0;
	for (; (i < u8LengthCpy - 1) && charPtrSrcCpy[i]; i++)
		charPtrDestCpy[i] = charPtrSrcCpy[i];

	charPtrDestCpy[i] = 0; // properly null-terminate the dest str
}

