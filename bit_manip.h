#ifndef BIT_MANIP_H_
#define BIT_MANIP_H_


/* brackets wrapping 'var', 'bitN' and 'val' in the macro expansion allow complex expressions
   without having to manually wrap them.
   ex: BIT_ASSIGN(ptr + 2, bit_num + 5, bit_val - 1);         // good
   vs. BIT_ASSIGN( (ptr + 2), (bit_num + 5), (bit_val - 1) ); // ugly
*/

#define BIT_SET(var, bitN)         ((var) |= (1 << (bitN)))
#define BIT_CLEAR(var, bitN)       ((var) &= ~(1 << (bitN)))
#define BIT_ASSIGN(var, bitN, val) (!!(val)) ? BIT_SET(var, bitN) : BIT_CLEAR(var, bitN)

#define BIT_TOGGLE(var, bitN)      ((var) ^= (1 << (bitN)))

#define BIT_GET(var, bitN)         (!!((var) & (1 << (bitN))))


#endif /* BIT_MANIP_H_ */

