#include "LCD_helper_functions.h"
#include "../bit_manip.h"
#include <util/delay.h>


// --- internal --- //
static void LCD_vidWaitWhileBusy(LCD_cptr_t structPtrLCDCpy)
{
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD4, INPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD5, INPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD6, INPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7, INPUT);

	// pin pull-up resistor doesn't have to be activated since each data pin will
	// always have an imposed value from the LCD.

	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRS, COMMAND);
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRW, READ);
	_delay_us(1); // RS and RW setup time

	register u8 isBusy;
	do
	{
		// --- cycle 1: read D7 - D4 --- //
		DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, ON);
		_delay_us(1); // data delay time

		isBusy = DIO_u8Get_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7);

		DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, OFF);
		_delay_us(1); // wait until LCD settles

		// --- cycle 2: read D3 - D0 --- //
		DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, ON);
		_delay_us(1); // data delay time

		DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, OFF);
		_delay_us(1); // data hold time + RS and RW setup time for next cycle
	}
	while (isBusy);

	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD4, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD5, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD6, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7, OUTPUT);
}

static void LCD_vidSendData_4bits_higher(const u8 u8CMDCpy, LCD_cptr_t structPtrLCDCpy)
{
	// prepare higher nibble
	DIO_vidSet_pinValue( structPtrLCDCpy->portData, structPtrLCDCpy->pinD7, BIT_GET(u8CMDCpy, 7) );
	DIO_vidSet_pinValue( structPtrLCDCpy->portData, structPtrLCDCpy->pinD6, BIT_GET(u8CMDCpy, 6) );
	DIO_vidSet_pinValue( structPtrLCDCpy->portData, structPtrLCDCpy->pinD5, BIT_GET(u8CMDCpy, 5) );
	DIO_vidSet_pinValue( structPtrLCDCpy->portData, structPtrLCDCpy->pinD4, BIT_GET(u8CMDCpy, 4) );

	// falling edge
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, ON);
	_delay_us(1); // data setup time
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, OFF);
	_delay_us(1); // data and RS/RW hold time + RS/RW setup time for the next write cycle
}

static void LCD_vidMagicInitSequence(LCD_cptr_t structPtrLCDCpy)
{
	// see Hitachi HD44780 datasheet p23 and p45 for info about this.
	// 3 times initialize to (8-bit mode, 1 line, 5x8 format),
	// after that, you can safely send as many commands/data as needed.

	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRS, COMMAND);
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRW, WRITE);
	_delay_us(1); // RS and RW setup time

	for (u8 i = 0; i < 3; i++)
	{
		// higher nibble only will be sent
		LCD_vidSendData_4bits_higher(Functions_Set_DL1_N0_F0, structPtrLCDCpy);

		_delay_ms(5); // wait until LCD settles
	}
}

static void LCD_vidSendDataOrCMD(const u8 u8DataCpy, const u8 u8DirectionCpy, LCD_cptr_t structPtrLCDCpy)
{
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRS, u8DirectionCpy);
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRW, WRITE);
	_delay_us(1); // RS and RW setup time

	// prepare higher nibble
	LCD_vidSendData_4bits_higher(u8DataCpy, structPtrLCDCpy);

	// prepare lower nibble
	LCD_vidSendData_4bits_higher(u8DataCpy << 4, structPtrLCDCpy);

	 // wait until LCD settles
	LCD_vidWaitWhileBusy(structPtrLCDCpy);
}

static u8 LCD_u8GetDataOrAC(const u8 u8DirectionCpy, LCD_cptr_t structPtrLCDCpy)
{
	register u8 read_data = 0;
	register u8 shift_base = 4;

	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD4, INPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD5, INPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD6, INPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7, INPUT);

	// pin pull-up resistor doesn't have to be activated since each data pin will
	// always have an imposed value from the LCD.

	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRS, u8DirectionCpy);
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRW, READ);
	_delay_us(1); // RS and RW setup time

	for (u8 i = 2; i > 0; i--)
	{
		DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, ON);
		_delay_us(1); // data delay time

		read_data |= DIO_u8Get_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7)
					 << (shift_base + 3);
		read_data |= DIO_u8Get_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD6)
					 << (shift_base + 2);
		read_data |= DIO_u8Get_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD5)
					 << (shift_base + 1);
		read_data |= DIO_u8Get_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD4)
					 << shift_base;

		DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, OFF);
		_delay_us(1); // data hold time + RS and RW setup time for next cycle

		shift_base = 0;
	}

	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD4, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD5, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD6, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7, OUTPUT);

	return read_data;
}

static inline u8 LCD_u8GetDataPointedToByAC(LCD_cptr_t structPtrLCDCpy)
{
	register u8 current_data = LCD_u8GetDataOrAC(DATA, structPtrLCDCpy);
	LCD_vidWaitWhileBusy(structPtrLCDCpy);

	return current_data;
}
// ---------------- //

void LCD_vidInit(LCD_cptr_t structPtrLCDCpy)
{
	// D4 - D7
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD4, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD5, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD6, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7, OUTPUT);

	DIO_vidSet_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD4, OFF);
	DIO_vidSet_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD5, OFF);
	DIO_vidSet_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD6, OFF);
	DIO_vidSet_pinValue(structPtrLCDCpy->portData, structPtrLCDCpy->pinD7, OFF);

	// RS, RW, E
	DIO_vidSet_pinDirection(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRS, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRW, OUTPUT);
	DIO_vidSet_pinDirection(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, OUTPUT);

	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRS, COMMAND);
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRW, WRITE);
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinE, OFF); // pull down pin E to prepare it for later use

	_delay_ms(80); // power-on delay (50ms - 100ms), see Hitachi HD44780 datasheet p45
	               //   ~40ms after Vcc reaches 2.7v
	               // + ~15ms after Vcc reaches 4.5v
	               // = ~55ms

	LCD_vidMagicInitSequence(structPtrLCDCpy);

	// ------------------------ Additional sequence for 4-bit mode ------------------------ //
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRS, COMMAND);
	DIO_vidSet_pinValue(structPtrLCDCpy->portCMD, structPtrLCDCpy->pinRW, WRITE);
	_delay_us(1); // RS and RW setup time

	// higher nibble
	LCD_vidSendData_4bits_higher(Functions_Set_DL0_N0_F0, structPtrLCDCpy);

	_delay_ms(5); // wait until LCD settles
	// ------------------------------------------------------------------------------------ //

	LCD_vidSendCMD(Functions_Set_DL0_N1_F0, structPtrLCDCpy);  // bus mode: 4-bit, line mode: 2, char format: 5x8
	LCD_vidSendCMD(Entry_mode_Set_ID1_SH0, structPtrLCDCpy);   // no shifting, move cursor right
	LCD_vidSendCMD(Clear_Display, structPtrLCDCpy);            // Address Counter (AC) = 0, DDRAM = ' '
	LCD_vidSendCMD(Display_Control_D1_C0_B0, structPtrLCDCpy); // display on, cursor off, blink off
}

inline void LCD_vidSendCMD(LCD_CMD_t enumCMDCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendDataOrCMD(enumCMDCpy, COMMAND, structPtrLCDCpy);
}

inline void LCD_vidWriteChar(const u8 u8CharCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendDataOrCMD(u8CharCpy, DATA, structPtrLCDCpy);
}

void LCD_vidWriteStr(const char* charPtrStrCpy, LCD_cptr_t structPtrLCDCpy)
{
	while (*charPtrStrCpy)
	{
		LCD_vidWriteChar(*charPtrStrCpy, structPtrLCDCpy);
		charPtrStrCpy++;
	}
}

void LCD_vidWriteInt(s32 s32NumCpy, LCD_cptr_t structPtrLCDCpy)
{
	s8 indexer = 0;

	if (s32NumCpy < 0)
	{
		s32NumCpy *= -1;
		LCD_vidWriteChar('-', structPtrLCDCpy);
	}

	u8 digits_buffer[10]; // 2^32 = 4294967296 (10 digits max)

	// store in buffer
	do
	{
		digits_buffer[indexer++] = (s32NumCpy % 10) + '0';
	}
	while (s32NumCpy /= 10);

	// write int from array backwards
	for (indexer--; indexer >= 0; indexer--)
		LCD_vidWriteChar(digits_buffer[indexer], structPtrLCDCpy);
}

void LCD_vidWriteFloat(f32 f32NumCpy, u8 u8AccuracyCpy, LCD_cptr_t structPtrLCDCpy)
{
	u8 isNegative = 0;

	if (f32NumCpy < 0)
	{
		isNegative = 1;
		f32NumCpy *= -1;
	}

	u32 int_part = (u32)f32NumCpy; // int part
	f32NumCpy -= int_part;         // float part

	// this prevents writing '-' if no accuracy required and int_part = 0
	if (isNegative)
	{
		if (int_part)
				LCD_vidWriteChar('-', structPtrLCDCpy);
		else if (u8AccuracyCpy && f32NumCpy)
			LCD_vidWriteChar('-', structPtrLCDCpy);
	}

	LCD_vidWriteInt(int_part, structPtrLCDCpy);

	// return if no accuracy required or no floating part
	if (!u8AccuracyCpy || !f32NumCpy)
		return;

	LCD_vidWriteChar('.', structPtrLCDCpy);

	// while there's is accuracy and floating part
	while (u8AccuracyCpy-- && f32NumCpy)
	{
		f32NumCpy *= 10; // move digit before floating point
		const u8 current_digit = (u8)f32NumCpy;
		LCD_vidWriteChar(current_digit + '0', structPtrLCDCpy);
		f32NumCpy -= current_digit; // remove digit before floating point
	}
}

void LCD_vidWriteByteAsBits(const u8 u8NumCpy, LCD_cptr_t structPtrLCDCpy)
{
	for (s8 i = sizeof(u8)*8 - 1; i >= 0; i--)
	{
		LCD_vidWriteChar( BIT_GET(u8NumCpy, i) + '0', structPtrLCDCpy );
	}
}

inline void LCD_vidWriteCharAt(const u8 u8CharCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidMoveCursorYX(u8LineCpy, u8ColCpy, structPtrLCDCpy);
	LCD_vidWriteChar(u8CharCpy, structPtrLCDCpy);
}

inline void LCD_vidWriteStrAt(const char* charPtrStrCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidMoveCursorYX(u8LineCpy, u8ColCpy, structPtrLCDCpy);
	LCD_vidWriteStr(charPtrStrCpy, structPtrLCDCpy);
}

inline void LCD_vidWriteIntAt(s32 s32NumCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidMoveCursorYX(u8LineCpy, u8ColCpy, structPtrLCDCpy);
	LCD_vidWriteInt(s32NumCpy, structPtrLCDCpy);
}

inline void LCD_vidWriteFloatAt(f32 f32NumCpy, u8 u8AccuracyCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidMoveCursorYX(u8LineCpy, u8ColCpy, structPtrLCDCpy);
	LCD_vidWriteFloat(f32NumCpy, u8AccuracyCpy, structPtrLCDCpy);
}

inline void LCD_vidWriteByteAsBitsAt(const u8 u8NumCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidMoveCursorYX(u8LineCpy, u8ColCpy, structPtrLCDCpy);
	LCD_vidWriteByteAsBits(u8NumCpy, structPtrLCDCpy);
}

inline void LCD_vidResetDisplay(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Clear_Display, structPtrLCDCpy);
}

inline void LCD_vidShiftDisplayLeft(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Cursor_Display_Shift_SC1_RL0, structPtrLCDCpy);
}

inline void LCD_vidShiftDisplayRight(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Cursor_Display_Shift_SC1_RL1, structPtrLCDCpy);
}

inline void LCD_vidMoveCursorHome(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Return_Home, structPtrLCDCpy);
}

inline void LCD_vidMoveCursorLeft(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Cursor_Display_Shift_SC0_RL0, structPtrLCDCpy);
}

inline void LCD_vidMoveCursorRight(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Cursor_Display_Shift_SC0_RL1, structPtrLCDCpy);
}

inline void LCD_vidMoveCursor1stLine(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Set_DDRAM_Addr_in_AC_2line_l1s, structPtrLCDCpy);
}

inline void LCD_vidMoveCursor2ndLine(LCD_cptr_t structPtrLCDCpy)
{
	LCD_vidSendCMD(Set_DDRAM_Addr_in_AC_2line_l2s, structPtrLCDCpy);
}

void LCD_vidMoveCursorYX(u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy)
{
	LCD_CMD_t lineCMD;

	// check for line
	if (u8LineCpy == 1)
		lineCMD = Set_DDRAM_Addr_in_AC_2line_l1s;
	else if (u8LineCpy == 2)
		lineCMD = Set_DDRAM_Addr_in_AC_2line_l2s;
	else
		return; // to prevent unpredicted behavior

	LCD_vidSendCMD(lineCMD + u8ColCpy - 1, structPtrLCDCpy);
}

inline void LCD_vidGotoCGRAMposition(const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy)
{
	if (u8ColCpy <= 64) // 2^6 = 64 (1 -> 64) (to prevent unpredicted behavior)
		LCD_vidSendCMD(Set_CGRAM_Addr_in_AC + u8ColCpy - 1, structPtrLCDCpy);
}

inline u8 LCD_u8GetCurrentAC(LCD_cptr_t structPtrLCDCpy)
{
	return LCD_u8GetDataOrAC(COMMAND, structPtrLCDCpy);
}

inline u8 LCD_u8GetCurrentDDRam(LCD_cptr_t structPtrLCDCpy)
{
	// see Hitachi HD44780 datasheet p31 for info about this.
	// cursor must be placed at the required position to be read, or else the read data will be invalid,
	// even if AC points to the required position!.

	// place AC at its current position, again!
	LCD_vidSendCMD( Set_DDRAM_Addr_in_AC_1line_s + LCD_u8GetCurrentAC(structPtrLCDCpy), structPtrLCDCpy );
	return LCD_u8GetDataPointedToByAC(structPtrLCDCpy);
}

inline u8 LCD_u8GetCurrentCGRam(LCD_cptr_t structPtrLCDCpy)
{
	// see Hitachi HD44780 datasheet p31 for info about this.
	// cursor must be placed at the required position to be read, or else the read data will be invalid,
	// even if AC points to the required position!.

	// place AC at its current position, again!
	LCD_vidSendCMD( Set_CGRAM_Addr_in_AC + LCD_u8GetCurrentAC(structPtrLCDCpy), structPtrLCDCpy );
	return LCD_u8GetDataPointedToByAC(structPtrLCDCpy);
}

