#ifndef LCD_FUNCTIONS_LCD_HELPER_FUNCTIONS_H_
#define LCD_FUNCTIONS_LCD_HELPER_FUNCTIONS_H_


#include "../avr_io_basics/DIO_functions/DIO_helper_functions.h"
#include "../custom_types.h"

// RS pin modes
#define COMMAND 0
#define DATA    1

// RW pin modes
#define WRITE 0
#define READ  1

typedef enum
{
	// bus mode: 4-bit, line mode: 1, char format: 5x8
	Functions_Set_DL0_N0_F0        = 0b00100000,
	// bus mode: 4-bit, line mode: 1, char format: 5x11
	Functions_Set_DL0_N0_F1        = 0b00100100,
	// bus mode: 4-bit, line mode: 2, char format: 5x8
	Functions_Set_DL0_N1_F0        = 0b00101000,
	// bus mode: 4-bit, line mode: 2, char format: 5x11
	Functions_Set_DL0_N1_F1        = 0b00101100,
	// bus mode: 8-bit, line mode: 1, char format: 5x8
	Functions_Set_DL1_N0_F0        = 0b00110000,
	// bus mode: 8-bit, line mode: 1, char format: 5x11
	Functions_Set_DL1_N0_F1        = 0b00110100,
	// bus mode: 8-bit, line mode: 2, char format: 5x8
	Functions_Set_DL1_N1_F0        = 0b00111000,
	// bus mode: 8-bit, line mode: 2, char format: 5x11
	Functions_Set_DL1_N1_F1        = 0b00111100,

	// --- how the cursor and display behave --- //
	// no shifting, move cursor left
	Entry_mode_Set_ID0_SH0         = 0b00000100,
	// no shifting, move cursor right
	Entry_mode_Set_ID1_SH0         = 0b00000110,
	// shift right, move cursor left
	Entry_mode_Set_ID0_SH1         = 0b00000101,
	// shift left, move cursor right
	Entry_mode_Set_ID1_SH1         = 0b00000111,
	// ----------------------------------------- //

	// DDRAM = ' ', Address Counter (AC) = 0
	Clear_Display                  = 0b00000001,
	// AC = 0
	Return_Home                    = 0b00000010,

	// ---
	// display off, cursor off, blink off
	Display_Control_D0_C0_B0       = 0b00001000,
	// display on, cursor off, blink off
	Display_Control_D1_C0_B0       = 0b00001100,
	// display on, cursor on, blink off
	Display_Control_D1_C1_B0       = 0b00001110,
	// display on, cursor off, blink on
	Display_Control_D1_C0_B1       = 0b00001101,
	// display on, cursor on, blink on
	Display_Control_D1_C1_B1       = 0b00001111,

	// AC = (DDRAM)0x00
	Set_DDRAM_Addr_in_AC_1line_s   = 0b10000000,
	// AC = (DDRAM)0x4F
	Set_DDRAM_Addr_in_AC_1line_e   = 0b10000000 | 0x4F,

	// AC = (DDRAM)0x00
	Set_DDRAM_Addr_in_AC_2line_l1s = 0b10000000,
	// AC = (DDRAM)0x27
	Set_DDRAM_Addr_in_AC_2line_l1e = 0b10000000 | 0x27,

	// AC = (DDRAM)0x40
	Set_DDRAM_Addr_in_AC_2line_l2s = 0b10000000 | 0x40,
	// AC = (DDRAM)0x67
	Set_DDRAM_Addr_in_AC_2line_l2e = 0b10000000 | 0x67,

	// AC = (CGRAM)0x00
	Set_CGRAM_Addr_in_AC = 0b01000000,

	// shift cursor left
	Cursor_Display_Shift_SC0_RL0   = 0b00010000,
	// shift cursor right
	Cursor_Display_Shift_SC0_RL1   = 0b00010100,
	// shift entire display left, shift cursor left
	Cursor_Display_Shift_SC1_RL0   = 0b00011000,
	// shift entire display right, shift cursor right
	Cursor_Display_Shift_SC1_RL1   = 0b00011100,
} LCD_CMD_t;

typedef struct
{
	DIO_port_t portCMD;  // Command port
	DIO_port_t portData; // Data-bus port

	u8 pinRS; // pin RS
	u8 pinRW; // pin RW
	u8 pinE;  // pin E

	// data-bus pins (D4 - D7)
	u8 pinD4;
	u8 pinD5;
	u8 pinD6;
	u8 pinD7;

	// --- private --- //
	//u8 charsWritten; // decided to leave this for the app layer
} LCD_obj_t;

typedef const LCD_obj_t* LCD_cptr_t;

void LCD_vidInit(LCD_cptr_t structPtrLCDCpy);

void LCD_vidSendCMD(LCD_CMD_t enumCMDCpy, LCD_cptr_t structPtrLCDCpy);

void LCD_vidWriteChar(const u8 u8CharCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteStr(const char* charPtrStrCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteInt(s32 s32NumCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteFloat(f32 f32NumCpy, u8 u8AccuracyCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteByteAsBits(const u8 u8NumCpy, LCD_cptr_t structPtrLCDCpy);

void LCD_vidWriteCharAt(const u8 u8CharCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteStrAt(const char* charPtrStrCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteIntAt(s32 s32NumCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteFloatAt(f32 f32NumCpy, u8 u8AccuracyCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy);
void LCD_vidWriteByteAsBitsAt(const u8 u8NumCpy, u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy);

void LCD_vidResetDisplay(LCD_cptr_t structPtrLCDCpy);
void LCD_vidShiftDisplayLeft(LCD_cptr_t structPtrLCDCpy);
void LCD_vidShiftDisplayRight(LCD_cptr_t structPtrLCDCpy);

void LCD_vidMoveCursorHome(LCD_cptr_t structPtrLCDCpy);
void LCD_vidMoveCursorLeft(LCD_cptr_t structPtrLCDCpy);
void LCD_vidMoveCursorRight(LCD_cptr_t structPtrLCDCpy);
void LCD_vidMoveCursor1stLine(LCD_cptr_t structPtrLCDCpy);
void LCD_vidMoveCursor2ndLine(LCD_cptr_t structPtrLCDCpy);
void LCD_vidMoveCursorYX(u8 u8LineCpy, const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy);

void LCD_vidGotoCGRAMposition(const u8 u8ColCpy, LCD_cptr_t structPtrLCDCpy);

u8 LCD_u8GetCurrentAC(LCD_cptr_t structPtrLCDCpy);
u8 LCD_u8GetCurrentDDRam(LCD_cptr_t structPtrLCDCpy);
u8 LCD_u8GetCurrentCGRam(LCD_cptr_t structPtrLCDCpy);


#endif /* LCD_FUNCTIONS_LCD_HELPER_FUNCTIONS_H_ */

