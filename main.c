// HAL
#include "Bluetooth_functions/Bluetooth_helper_functions.h"
#include "LCD_functions/LCD_helper_functions.h"
// Services Layer
#include "Services/str_manip/str_functions.h"
#include <util/delay.h>
// App Layer (helper functions for the App Layer)
#include "Users_db/Users_db_funcs.h"
#include "Action_functions/z_functions.h"


// --- global objects --- //
LCD_obj_t lcd; // LCD object
#define Rx_DATA_BUFFER_LENGTH 150
char Rx_data_buffer[Rx_DATA_BUFFER_LENGTH]; // buffer to store data bytes from BT
// application layer callback function,
// called by when a complete string is received
void (*APP_CB_func)(void);
s8 last_valid_user; // indicator of last valid user, used by some call-backs
// ----------------- //


// --- callbacks --- //
void APP_CB_vidBT_RxByte(u8 byte); // initial receiver for user bytes: after receiving a complete string from user,
                                   // this function will call other callbacks to handle different situations/events
                                   // such as: check login name and pass, change name and pass, handle user commands, etc...

void APP_CB_vidInitCheckUserName(void); // 1) callback used to check user name
void APP_CB_vidInitCheckUserPass(void); // 2) callback used to check user password

void APP_CB_vidHandleUserCMD(void);     // 3) callback used to process user commands

void APP_CB_vidDBchangeUserName(void); // callback used to change user name in db (database)
void APP_CB_vidDBchangeUserPass(void); // callback used to change user password in db
// ----------------- //


void main(void)
{
	// init LCD
	lcd.portCMD  = port_A;
	lcd.portData = port_A;
	lcd.pinRS = 0;
	lcd.pinRW = 1;
	lcd.pinE  = 2;
	lcd.pinD4 = 3;
	lcd.pinD5 = 4;
	lcd.pinD6 = 5;
	lcd.pinD7 = 6;

	LCD_vidInit(&lcd);

	/*
	Keypad_obj_t keypad;
	keypad.portRows = port_C;
	keypad.portCols = port_C;
	keypad.pinsR[0] = ;
	keypad.pinsR[1] = ;
	keypad.pinsR[2] = ;
	keypad.pinsR[3] = ;
	keypad.pinsC[0] = ;
	keypad.pinsC[1] = ;
	keypad.pinsC[2] = ;
	keypad.pinsC[3] = ;
*/
	// warn the user that he should disconnect
	LCD_vidWriteStr("Disconnect BT...", &lcd);

	// 3 sec count down
	for (u8 i = 3; i > 0; i--)
	{
		LCD_vidWriteCharAt('0' + i, 2, 1, &lcd);
		_delay_ms(1000);
	}

	// tell the user we are preparing some stuf...
	LCD_vidResetDisplay(&lcd);
	LCD_vidWriteStr("Initializing...", &lcd);


	// init BT
	if ( BT_u8init(port_D, 7, "CM-Box", "4763") ) // BT was initialized
	{
		LCD_vidWriteStrAt("BT OK.", 2, 1, &lcd);
		_delay_ms(1000);
	}
	else                                          // BT was not initialized
	{
		LCD_vidWriteStrAt("BT ERROR.", 2, 1, &lcd);

		// trap to prevent further processing
		while (1)
		{}
	}


	// init users
	Users_vidInit();
	LCD_vidWriteStrAt("Users DB loaded.", 2, 1, &lcd);
	_delay_ms(1500);


	// register a callback function that receives a (data) byte from BT
	BT_vidRegisterCB(APP_CB_vidBT_RxByte);

	// register the first callback function (check user name)
	APP_CB_func = APP_CB_vidInitCheckUserName;

	// tell the user he can pair & connect now
	LCD_vidResetDisplay(&lcd);
	LCD_vidWriteStr("Pair / Connect !", &lcd);

	while (1)
	{

	}

}


// called when a byte is received from BT
void APP_CB_vidBT_RxByte(u8 byte)
{
	 // indexer for the Rx buffer
	static u8 i = 0;

	// if ('\r' is received), then skip/ignore this turn
	if (byte == '\r')
		return;

	// if ('\n' is received) or (we reached the last char),
	// then this is considered a termination signal
	if ((byte == '\n') || (i == Rx_DATA_BUFFER_LENGTH - 1))
	{
		Rx_data_buffer[i] = 0; // properly terminate the string with null for later processing
		i = 0; // reset indexer to prepare for a new data/CMD

		if (APP_CB_func) // if callback is registered (not empty)
			APP_CB_func(); // process what we received

		return;
	}

	Rx_data_buffer[i++] = byte; // store the received byte
}

// 1) callback used to check user name
void APP_CB_vidInitCheckUserName(void)
{
	static u8 trials = 3;

	last_valid_user = User_s8isNameExist(Rx_data_buffer);

	if (last_valid_user != -1) // existing user (correct input)
	{
		trials = 3; // reset trials counter

		// register next callback function (check user password)
		APP_CB_func = APP_CB_vidInitCheckUserPass;

		BT_vidSendStr("Password:", 1);

		BUZZ_vidCorrect();
	}
	else                       // non-existing user (bad input)
	{
		trials--; // dec trials counter


		BT_vidSendStr("ERROR: invalid user name!", 1);

		BUZZ_vidWorng();
	}

	if (!trials)               // if max attempts reached (worst input)
	{
		trials = 3; // reset trials counter

		last_valid_user = -1;

		BT_vidSendStr("ERROR: max. attempts reached!", 1);

		BUZZ_vidWorng();
	}
}

// 2) callback used to check user password
void APP_CB_vidInitCheckUserPass(void)
{
	static u8 trials = 3;

	if (User_u8isPasswordValid(Rx_data_buffer, last_valid_user)) // valid password (correct input)
	{
		trials = 3; // reset trials counter

		// register next callback function (handle user commands)
		APP_CB_func = APP_CB_vidHandleUserCMD;

		BT_vidSendStr("Welcome!", 1);
		BT_vidSendStr("Type the desired command:", 1);

		BUZZ_vidCorrect();
	}
	else                                                        // invalid password (bad input)
	{
		trials--; // dec trials counter

		BT_vidSendStr("ERROR: invalid password!", 1);

		BUZZ_vidWorng();
	}

	if (!trials)                                                  // if max attempts reached (worst input)
	{
		trials = 3; // reset trials counter

		last_valid_user = -1;

		// register next callback function (check user name)
		APP_CB_func = APP_CB_vidInitCheckUserName;

		BT_vidSendStr("ERROR: max. attempts reached!", 1);
		BT_vidSendStr("User name:", 1);

		BUZZ_vidWorng();
	}

}

// 3) callback used to process user commands
void APP_CB_vidHandleUserCMD(void)
{
	if (str_u8StrCmp(Rx_data_buffer, "$un=")) // new user-name request
	{
		// register next callback function (change user name)
		APP_CB_func = APP_CB_vidDBchangeUserName;

		BT_vidSendStr("New user name:", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "$pw=")) // new password request
	{
		// register next callback function (change user password)
		APP_CB_func = APP_CB_vidDBchangeUserPass;

		BT_vidSendStr("New password:", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "$logout")) // logout request
	{
		// register next callback function (check user name)
		APP_CB_func = APP_CB_vidInitCheckUserName;

		last_valid_user = -1;

		BT_vidSendStr("Logged out!", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "#unlk")) // activate solenoid
	{
		// register next callback function (change user password)
		APP_CB_func = APP_CB_vidHandleUserCMD; // redundant, but left for clarity
                                               // APP_CB_func won't change if this line ommitted

		Solenoid_vidOpen();

		BT_vidSendStr("Door unlocked", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "#lk")) // deactivate solenoid
	{
		// register next callback function (change user password)
		APP_CB_func = APP_CB_vidHandleUserCMD; // redundant, but left for clarity
		                                       // APP_CB_func won't change if this line ommitted

		Solenoid_vidClosed();

		BT_vidSendStr("Door locked", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "#tg")) // toggle solenoid
	{
		// register next callback function (change user password)
		APP_CB_func = APP_CB_vidHandleUserCMD; // redundant, but left for clarity
                                               // APP_CB_func won't change if this line ommitted

		Solenoid_vidToggel();

		BT_vidSendStr("Door toggled", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "#on")) // activate Light
	{
		// register next callback function (change user password)
		APP_CB_func = APP_CB_vidHandleUserCMD; // redundant, but left for clarity
                                               // APP_CB_func won't change if this line ommitted

		Light_ON();

		BT_vidSendStr("Lamp On", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "#off")) // deactivate Light
	{
		// register next callback function (change user password)
		APP_CB_func = APP_CB_vidHandleUserCMD; // redundant, but left for clarity
                                               // APP_CB_func won't change if this line ommitted

		Light_OFF();

		BT_vidSendStr("Lamp OFF", 1);
	}
	else if (str_u8StrCmp(Rx_data_buffer, "#t")) // deactivate Light
	{
		// register next callback function (change user password)
		APP_CB_func = APP_CB_vidHandleUserCMD; // redundant, but left for clarity
                                               // APP_CB_func won't change if this line ommitted

		Light_Toggle();

		BT_vidSendStr("Toggle Lamp", 1);
	}
	else                                               // undefined cmd (this must be the last if statement)
	{
		BT_vidSendStr("Unknown command.", 1);
	}

}

// callback used to change user name in db (database)
void APP_CB_vidDBchangeUserName(void)
{
	static char previous_input[16 + 1] = {0}; // stores first input
	static u8 isPreviousInput = 0;            // was this function called previously ?

	static u8 trials = 3;

	if (!isPreviousInput) // new input
	{
		isPreviousInput = 1;

		// copy new user name
		str_vidStrCopy(previous_input, sizeof(previous_input), Rx_data_buffer);
		BT_vidSendStr("Confirm user name:", 1);
	}
	else                  // next (2nd) input
	{
		if (str_u8StrCmp(Rx_data_buffer, previous_input)) // valid user name (correct input)
		{
			trials = 3; // reset trials counter

			isPreviousInput = 0; // reset is previous input indicator/flag

			// register next callback function (handle user commands)
			APP_CB_func = APP_CB_vidHandleUserCMD;

			User_vidChangeName(Rx_data_buffer, last_valid_user); // change user name in db
			User_vidFlushUserData(last_valid_user);              // flush/write new user info to EEPROM

			BT_vidSendStr("SUCCESS: user name changed!", 1);

			BUZZ_vidCorrect();
		}
		else                                               // invalid user name (bad input)
		{
			trials--; // dec trials counter

			BT_vidSendStr("ERROR: user name mismatch.", 1);

			BUZZ_vidWorng();
		}

		if (!trials)                                       // if max attempts reached (worst input)
		{
			trials = 3; // reset trials counter

			isPreviousInput = 0; // reset is previous input indicator/flag

			last_valid_user = -1;

			// register next callback function (check user name)
			APP_CB_func = APP_CB_vidInitCheckUserName;

			BT_vidSendStr("ERROR: max. attempts reached!", 1);
			BT_vidSendStr("Logged out!", 1);

			BUZZ_vidWorng();
		}
	}

}

// callback used to change user password in db
void APP_CB_vidDBchangeUserPass(void)
{
	static char previous_input[6 + 1] = {0}; // stores first input
	static u8 isPreviousInput = 0;           // was this function called previously ?

	static u8 trials = 3;

	if (!isPreviousInput) // new input
	{
		isPreviousInput = 1;

		// copy new user name
		str_vidStrCopy(previous_input, sizeof(previous_input), Rx_data_buffer);
		BT_vidSendStr("Confirm user password:", 1);
	}
	else                  // next (2nd) input
	{
		if (str_u8StrCmp(Rx_data_buffer, previous_input)) // valid password (correct input)
		{
			trials = 3; // reset trials counter

			isPreviousInput = 0; // reset is previous input indicator/flag

			// register next callback function (check user name)
			APP_CB_func = APP_CB_vidInitCheckUserName;

			User_vidChangePassword(Rx_data_buffer, last_valid_user);
			User_vidFlushUserData(last_valid_user);

			last_valid_user = -1; // change this after flushing user data

			BT_vidSendStr("SUCCESS: password changed!", 1);
			BT_vidSendStr("Please login again.", 1);

			BUZZ_vidCorrect();
		}
		else                                               // invalid password (bad input)
		{
			trials--; // dec trials counter

			BT_vidSendStr("ERROR: password mismatch.", 1);

			BUZZ_vidWorng();
		}

		if (!trials)                                       // if max attempts reached (worst input)
		{
			trials = 3; // reset trials counter

			isPreviousInput = 0; // reset is previous input indicator/flag

			last_valid_user = -1;

			// register next callback function (check user name)
			APP_CB_func = APP_CB_vidInitCheckUserName;

			BT_vidSendStr("ERROR: max. attempts reached!", 1);
			BT_vidSendStr("Logged out!", 1);

			BUZZ_vidWorng();
		}
	}

}

