#ifndef BLUETOOTH_FUNCTIONS_BLUETOOTH_HELPER_FUNCTIONS_H_
#define BLUETOOTH_FUNCTIONS_BLUETOOTH_HELPER_FUNCTIONS_H_


#include "../custom_types.h"
#include "../avr_io_basics/USART_functions/USART_helper_functions.h"
#include "../avr_io_basics/DIO_functions/DIO_helper_functions.h"

typedef enum
{
    BT_role_slave,
    BT_role_master
} BT_role_t;

typedef void (*BT_CB_t)(u8); // callback for when a byte is received

#define PORT_UNUSED 0xFF
#define PIN_UNUSED  0xFF

u8 BT_u8init(DIO_port_t enumPortCMDCpy, const u8 u8PinCMDCpy, const char* charPtrNameCpy, const char* charPtrPINCpy);

void BT_vidSendCMD(const char* charPtrCMDCpy);
void BT_vidSendChar(const u8 u8DataCpy, u8 u8isTerminateCpy);
void BT_vidSendStr(const char* charPtrDataCpy, u8 u8isTerminateCpy);

u8 BT_u8TestUART(void);

void BT_vidRegisterCB(BT_CB_t funcPtrCpy);
void BT_vidDeregisterCB(void);

void BT_vidReset(void);

void BT_vidSetRole(BT_role_t enumRoleCpy);

void BT_vidChangeName(const char* charPtrNameCpy);
void BT_vidChangePIN(const char charPtrPINCpy[4]);


#endif /* BLUETOOTH_FUNCTIONS_BLUETOOTH_HELPER_FUNCTIONS_H_ */

