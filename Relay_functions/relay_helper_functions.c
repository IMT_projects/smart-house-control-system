#include "relay_helper_functions.h"


void Relay_vidInit(Relay_cptr_t structPtrRelayCpy)
{
    DIO_vidSet_pinDirection(structPtrRelayCpy->portRelay, structPtrRelayCpy->pinRelay, OUTPUT);
    DIO_vidSet_pinValue(structPtrRelayCpy->portRelay, structPtrRelayCpy->pinRelay, structPtrRelayCpy->Relay_off_state);
}

void Relay_vidActivate(Relay_cptr_t structPtrRelayCpy)
{
    DIO_vidSet_pinValue(structPtrRelayCpy->portRelay, structPtrRelayCpy->pinRelay, !(structPtrRelayCpy->Relay_off_state));
}

void Relay_vidDeactivate(Relay_cptr_t structPtrRelayCpy)
{
    DIO_vidSet_pinValue(structPtrRelayCpy->portRelay, structPtrRelayCpy->pinRelay, structPtrRelayCpy->Relay_off_state);
}

void Relay_vidToggle(Relay_cptr_t structPtrRelayCpy)
{
    DIO_vidToggle_pinValue(structPtrRelayCpy->portRelay, structPtrRelayCpy->pinRelay);
}

