#ifndef RELAY_FUNCTIONS_RELAY_HELPER_FUNCTIONS_H_
#define RELAY_FUNCTIONS_RELAY_HELPER_FUNCTIONS_H_


#include "../avr_io_basics/DIO_functions/DIO_helper_functions.h"
#include "../custom_types.h"

typedef struct
{
    DIO_port_t portRelay; // Relay port
    u8 pinRelay;          // Relay pin
    u8 Relay_off_state;   // Relay off state: ON, OFF
} Relay_obj_t;

typedef const Relay_obj_t* Relay_cptr_t;


void Relay_vidInit(Relay_cptr_t structPtrRelayCpy);

void Relay_vidActivate(Relay_cptr_t structPtrRelayCpy);
void Relay_vidDeactivate(Relay_cptr_t structPtrRelayCpy);

void Relay_vidToggle(Relay_cptr_t structPtrRelayCpy);


#endif /* RELAY_FUNCTIONS_RELAY_HELPER_FUNCTIONS_H_ */

