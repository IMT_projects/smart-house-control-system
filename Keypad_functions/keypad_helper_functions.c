#include "keypad_helper_functions.h"
#include <util/delay.h>


// --- internal --- //
static const u8 Keypad_keysMap[KEYPAD_ROWS_COUNT_][KEYPAD_COLS_COUNT_] = {
		{'7', '8', '9', '+'}, // row 0, column 0
		{'4', '5', '6', '-'},
		{'1', '2', '3', '*'},
		{'=', '0', '.', '/'}
};
// ---------------- //

void Keypad_vidInit(Keypad_cptr_t structPtrKeypadCpy)
{
	for (u8 i = 0; i < KEYPAD_COLS_COUNT_; i++)
	{
		DIO_vidSet_pinDirection(structPtrKeypadCpy->portCols, structPtrKeypadCpy->pinsC[i], INPUT); // col

		DIO_vidActivate_pinPullUp(structPtrKeypadCpy->portCols, structPtrKeypadCpy->pinsC[i]);
	}

	for (u8 i = 0; i < KEYPAD_ROWS_COUNT_; i++)
	{
		DIO_vidSet_pinDirection(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[i], OUTPUT); // row

		DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[i], ON);
	}

}

u8 Keypad_u8GetPressedKey(u16 u16Timeout_msCpy, Keypad_cptr_t structPtrKeypadCpy)
{
	while (1)
	{
		for (u8 r = 0; r < KEYPAD_ROWS_COUNT_; r++) // foreach row
		{
			DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[r], OFF); // turn off current row

			for (u8 c = 0; c < KEYPAD_COLS_COUNT_; c++) // foreach col
			{
				if (DIO_u8Get_pinValue(structPtrKeypadCpy->portCols, structPtrKeypadCpy->pinsC[c]) == 0) // if key pressed
				{
					DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[r], ON); // turn back on current row
					return Keypad_keysMap[r][c];
				}
			}

			DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[r], ON); // turn back on current row
		}

		if (u16Timeout_msCpy--)
			_delay_ms(1);
		else
			break;
	}

	return 0; // nothing pressed
}

