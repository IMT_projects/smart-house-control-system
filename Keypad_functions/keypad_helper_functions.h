#ifndef KEYPAD_FUNCTIONS_KEYPAD_HELPER_FUNCTIONS_H_
#define KEYPAD_FUNCTIONS_KEYPAD_HELPER_FUNCTIONS_H_


#include "../avr_io_basics/DIO_functions/DIO_helper_functions.h"
#include "../custom_types.h"

#define KEYPAD_ROWS_COUNT_ 4 // number of rows
#define KEYPAD_COLS_COUNT_ 4 // number of columns

typedef struct
{
	DIO_port_t portRows; // rows port
	DIO_port_t portCols; // columns port

	// rows (output)
	u8 pinsR[KEYPAD_ROWS_COUNT_];

	// columns (input)
	u8 pinsC[KEYPAD_COLS_COUNT_];
} Keypad_obj_t;

typedef const Keypad_obj_t* Keypad_cptr_t;

void Keypad_vidInit(Keypad_cptr_t structPtrKeypadCpy);
u8 Keypad_u8GetPressedKey(u16 u16Timeout_msCpy, Keypad_cptr_t structPtrKeypadCpy);


#endif /* KEYPAD_FUNCTIONS_KEYPAD_HELPER_FUNCTIONS_H_ */

