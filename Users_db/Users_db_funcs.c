#include "Users_db_funcs.h"
#include "../avr_io_basics/EEPROMi_functions/EEPROMi_helper_functions.h"
#include "../Services/str_manip/str_functions.h"
#include <util/delay.h>


// --- internal --- //
typedef struct
{
	char Name[16 + 1];
	char Password[6 + 1];
	u8 isInitialised; // must be last element
} User_t;

// users array (how many users)
#define USERS_LENGTH 10
static User_t User[USERS_LENGTH];

// write user data to EEPROM
static inline void User_vidFlushUserData2EEPROMi(const u8 u8UserNumCpy)
{
	// store user data in EEPROM
	EEPROMi_vidWriteArray( &(User[u8UserNumCpy]), sizeof(User_t), u8UserNumCpy*sizeof(User_t) );
}

// read user data from EEPROM
static inline void User_vidGetUserDataFromEEPROMi(const u8 u8UserNumCpy)
{
	// fill user data from EEPROM
	EEPROMi_vidReadArray( &(User[u8UserNumCpy]), sizeof(User_t), u8UserNumCpy*sizeof(User_t) );
}
// ---------------- //

// init users (write if new, read if already exist)
void Users_vidInit(void)
{
	// get/init user data
	for (u8 i = 0; i < USERS_LENGTH; i++)
	{
		if (   EEPROMi_u8ReadByte( (i+1)*sizeof(User_t) - 1  ) != 1      ) // if (not initialized), then init data
		{
			// change user data, ex: "user 1"
			User_vidChangeName("user  ", i);
			User[i].Name[5] = i + '0';
			User_vidChangePassword("123", i);
			User[i].isInitialised = 1;

			// store data in EEPROM
			User_vidFlushUserData2EEPROMi(i);
		}
		else                                                      // if (already initialized), then get data
		{
			// fill data from EEPROM
			User_vidGetUserDataFromEEPROMi(i);
		}
	}
}

// --- db configuration --- //
void User_vidChangeName(const char* charPtrNameCpy, const u8 u8UserNumCpy)
{
	if (u8UserNumCpy >= USERS_LENGTH)
		return;

	u8 i;
	for (i = 0; (i < 16) && charPtrNameCpy[i]; i++)
		User[u8UserNumCpy].Name[i] = charPtrNameCpy[i];

	// properly terminate string
	User[u8UserNumCpy].Name[i] = 0;
}

void User_vidChangePassword(const char* charPtrPassCpy, const u8 u8UserNumCpy)
{
	if (u8UserNumCpy >= USERS_LENGTH)
		return;

	u8 i;
	for (i = 0; (i < 6) && charPtrPassCpy[i]; i++)
		User[u8UserNumCpy].Password[i] = charPtrPassCpy[i];

	// properly terminate string
	User[u8UserNumCpy].Password[i] = 0;
}

// write a specific user data (by index) to EEPROM
void User_vidFlushUserData(const u8 u8UserNumCpy)
{
	if (u8UserNumCpy >= USERS_LENGTH)
		return;

	User_vidFlushUserData2EEPROMi(u8UserNumCpy);
}

// write all users data to EEPROM
void User_vidFlushAllUserData(void)
{
	for (u8 i = 0; i < USERS_LENGTH; i++)
		User_vidFlushUserData2EEPROMi(i);
}
// ------------------------ //

// --- db interface ---//
// check if user exist (by name) and return his index
s8 User_s8isNameExist(const char* charPtrNameCpy)
{
	for (u8 i = 0; i < USERS_LENGTH; i++) // foreach user
	{
		 // if (name match found), then return user index
		if ( str_u8StrCmp(User[i].Name, charPtrNameCpy) )
			return i;
	}

	// if name doesn't exist in db
	return -1;
}

// check if password of provided user index is valid
inline u8 User_u8isPasswordValid(const char* charPtrPassCpy, const u8 u8UserNumCpy)
{
	return str_u8StrCmp(User[u8UserNumCpy].Password, charPtrPassCpy);
}
// --------------------//

