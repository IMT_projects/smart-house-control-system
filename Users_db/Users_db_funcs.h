#ifndef USERS_DB_USERS_DB_FUNCS_H_
#define USERS_DB_USERS_DB_FUNCS_H_


#include "../custom_types.h"

// init users (write if new, read if already exist)
void Users_vidInit(void);

// --- db configuration --- //
void User_vidChangeName(const char* charPtrNameCpy, const u8 u8UserNumCpy);

void User_vidChangePassword(const char* charPtrPassCpy, const u8 u8UserNumCpy);

// write a specific user data (by index) to EEPROM
void User_vidFlushUserData(const u8 u8UserNumCpy);

// write all users data to EEPROM
void User_vidFlushAllUserData(void);
// ------------------------ //

// --- db interface ---//
// check if user exist (by name) and return his index
s8 User_s8isNameExist(const char* charPtrNameCpy);

// check if password of provided user index is valid
u8 User_u8isPasswordValid(const char* charPtrPassCpy, const u8 u8UserNumCpy);
// --------------------//


#endif /* USERS_DB_USERS_DB_FUNCS_H_ */

