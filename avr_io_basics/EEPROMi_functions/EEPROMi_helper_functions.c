#include "EEPROMi_helper_functions.h"
#include "../io_addrs.h"
#include "../../bit_manip.h"
#include "../INT_functions/INT_helper_functions.h"


#define EEPROMi_ADDRS_LENGTH 1024

void EEPROMi_vidInit(void)
{
    // disable EEPROM Ready Interrupt
    BIT_CLEAR(EEPROMi_CTRL, 3);

    // disable EEPROM Master Write Enable
    BIT_CLEAR(EEPROMi_CTRL, 2);
}

void EEPROMi_vidWriteByte(const u8 u8DataCpy, const u16 u16AddrCpy)
{
    if (u16AddrCpy >= EEPROMi_ADDRS_LENGTH)
        return;

    // 1) wait until Write Enable becomes zero
    while (BIT_GET(EEPROMi_CTRL, 1))
    {}
    // ------------------------ //

    // 2) write new address and data
    EEPROMi_ADDRESS = u16AddrCpy;
    EEPROMi_DATA    = u8DataCpy;
    // ------------------------ //

    // 3) set Master Write Enable bit to 1 while writing 0 to Write Enable bit
    //    INTs must be disabled since next 2 operations are dangerous

    // disable INTs
    volatile u8 mcu_status_b7 = (MCU_STATUS & 0b10000000);
    INT_vidDisable_global_flag();

    // get/clone EEPROMi status register
    volatile u8 eepromi_ctrl_ = EEPROMi_CTRL;

    // modify cloned EEPROMi status register as needed
    BIT_SET(eepromi_ctrl_, 2);   // write 1 to Master Write Enable bit
    BIT_CLEAR(eepromi_ctrl_, 1); // write 0 to Write Enable bit

    // set EEPROMi status register to the modified version
    EEPROMi_CTRL = eepromi_ctrl_;
    // ------------------------ //

    // 4) write 1 to Write Enable bit
    BIT_SET(EEPROMi_CTRL, 1);

    // wait until Write Enable becomes zero
    while (BIT_GET(EEPROMi_CTRL, 1))
    {}
    // ------------------------ //

    // restore INTs to original state
    MCU_STATUS |= mcu_status_b7;
}

void EEPROMi_vidWriteStr(const char* u8StrCpy, u16 u16AddrCpy)
{
    for (u16 i = 0; (u16AddrCpy < EEPROMi_ADDRS_LENGTH) && u8StrCpy[i]; i++, u16AddrCpy++)
        EEPROMi_vidWriteByte(u8StrCpy[i], u16AddrCpy);
}

void EEPROMi_vidWriteArray(const void* vidPtrArrCpy, u16 u16LengthCpy, u16 u16AddrCpy)
{
    for (u16 i = 0; (i < u16LengthCpy) && (u16AddrCpy < EEPROMi_ADDRS_LENGTH); i++, u16AddrCpy++)
        EEPROMi_vidWriteByte( ( (u8*)vidPtrArrCpy )[i], u16AddrCpy );
}

u8 EEPROMi_u8ReadByte(const u16 u16AddrCpy)
{
    if (u16AddrCpy >= EEPROMi_ADDRS_LENGTH)
        return 0;

    // 1) wait until Write Enable becomes zero
    while (BIT_GET(EEPROMi_CTRL, 1))
    {}
    // ------------------------ //

    // 2) set address
    EEPROMi_ADDRESS = u16AddrCpy;
    // ------------------------ //

    // 3) set EEPROM Read Enable
    //    INTs must be disabled since this operation is dangerous

    // disable INTs
    volatile u8 mcu_status_b7 = (MCU_STATUS & 0b10000000);
    INT_vidDisable_global_flag();

    BIT_SET(EEPROMi_CTRL, 0);
    // ------------------------ //

    // restore INTs to original state
    MCU_STATUS |= mcu_status_b7;

    return EEPROMi_DATA;
}

void EEPROMi_vidReadArray(void* vidPtrArrCpy, u16 u16LengthCpy, u16 u16AddrCpy)
{
    for (u16 i = 0; (i < u16LengthCpy) && (u16AddrCpy < EEPROMi_ADDRS_LENGTH); i++, u16AddrCpy++)
        ( (u8*)vidPtrArrCpy )[i] = EEPROMi_u8ReadByte(u16AddrCpy);
}

