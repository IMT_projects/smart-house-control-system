#ifndef AVR_IO_BASICS_EEPROMI_FUNCTIONS_EEPROMI_HELPER_FUNCTIONS_H_
#define AVR_IO_BASICS_EEPROMI_FUNCTIONS_EEPROMI_HELPER_FUNCTIONS_H_


#include  "../../custom_types.h"

void EEPROMi_vidInit(void);

void EEPROMi_vidWriteByte(const u8 u8DataCpy, const u16 u16AddrCpy);
void EEPROMi_vidWriteStr(const char* u8StrCpy, u16 u16AddrCpy);
void EEPROMi_vidWriteArray(const void* vidPtrArrCpy, u16 u16LengthCpy, u16 u16AddrCpy);

u8 EEPROMi_u8ReadByte(const u16 u16AddrCpy);
void EEPROMi_vidReadArray(void* vidPtrArrCpy, u16 u16LengthCpy, u16 u16AddrCpy);


#endif /* AVR_IO_BASICS_EEPROMI_FUNCTIONS_EEPROMI_HELPER_FUNCTIONS_H_ */

