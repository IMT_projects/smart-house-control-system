#ifndef IO_HELPER_FUNCTIONS_H
#define IO_HELPER_FUNCTIONS_H


#include "../../custom_types.h"

// now it's easier to remember!
#define INPUT  0
#define OUTPUT 1

// more descriptive!
#define OFF    0
#define ON     1
#define ON_ALL 0xFF

typedef enum
{
	port_A = 0,
	port_B,
	port_C,
	port_D
} DIO_port_t;

void DIO_vidInit();

// --- setters --- //
void DIO_vidSet_portDirection(DIO_port_t enumPortCpy, u8 u8StateCpy);
void DIO_vidSet_pinDirection(DIO_port_t enumPortCpy, const u8 u8PinNCpy, const u8 u8StateCpy);

void DIO_vidSet_portValue(DIO_port_t enumPortCpy, const u8 u8ValCpy);
void DIO_vidSet_pinValue(DIO_port_t enumPortCpy, const u8 u8PinNCpy, const u8 u8StateCpy);

void DIO_vidToggle_portValue(DIO_port_t enumPortCpy);
void DIO_vidToggle_pinValue(DIO_port_t enumPortCpy, const u8 u8PinNCpy);

void DIO_vidActivate_portPullUp(DIO_port_t enumPortCpy);
void DIO_vidDeactivate_portPullUp(DIO_port_t enumPortCpy);

void DIO_vidActivate_pinPullUp(DIO_port_t enumPortCpy, const u8 u8PinNCpy);
void DIO_vidDeactivate_pinPullUp(DIO_port_t enumPortCpy, const u8 u8PinNCpy);
// --------------- //

// --- getters --- //
u8 DIO_u8Get_portValue(DIO_port_t enumPortCpy);
u8 DIO_u8Get_pinValue(DIO_port_t enumPortCpy, const u8 u8PinNCpy);

u8 DIO_u8Wait_portInput(DIO_port_t enumPortCpy, u8* u8PtrChangedPinCpy, u16 u16Timeout_msCpy);
u8 DIO_u8Wait_pinInput(DIO_port_t enumPortCpy, const u8 u8PinNCpy, u8* isPinChangedCpy, u16 u16Timeout_msCpy);
// --------------- //


#endif // IO_HELPER_FUNCTIONS_H

