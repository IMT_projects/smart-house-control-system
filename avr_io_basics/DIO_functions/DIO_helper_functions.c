#include "../io_addrs.h"
#include "../../bit_manip.h"
#include <util/delay.h>
#include "DIO_helper_functions.h"


inline void DIO_vidInit()
{
    // TODO: complete this
	ADC_CTRL_STATUS &= 0b01111111; // turn off ADC, allowing port_A to be used
}

// --- setters --- //
void DIO_vidSet_portDirection(DIO_port_t enumPortCpy, u8 u8StateCpy)
{
	u8StateCpy = (!!u8StateCpy) ? ON_ALL : OFF;

	switch (enumPortCpy)
	{
		case port_A:
			DDR_A = u8StateCpy;
		break;

		case port_B:
			DDR_B = u8StateCpy;
		break;

		case port_C:
			DDR_C = u8StateCpy;
		break;

		case port_D:
			DDR_D = u8StateCpy;
		break;
	}
}

void DIO_vidSet_pinDirection(DIO_port_t enumPortCpy, const u8 u8PinNCpy, const u8 u8StateCpy)
{
	volatile u8* ddrX;

	switch (enumPortCpy)
	{
		case port_A:
			ddrX = &DDR_A;
		break;

		case port_B:
			ddrX = &DDR_B;
		break;

		case port_C:
			ddrX = &DDR_C;
		break;

		case port_D:
			ddrX = &DDR_D;
		break;

		default:
		return;
	}

	BIT_ASSIGN(*ddrX, u8PinNCpy, u8StateCpy);
}

inline void DIO_vidSet_portValue(DIO_port_t enumPortCpy, const u8 u8ValCpy)
{
	switch (enumPortCpy)
	{
		case port_A:
			PORT_A = u8ValCpy;
		break;

		case port_B:
			PORT_B = u8ValCpy;
		break;

		case port_C:
			PORT_C = u8ValCpy;
		break;

		case port_D:
			PORT_D = u8ValCpy;
		break;
	}
}

void DIO_vidSet_pinValue(DIO_port_t enumPortCpy, const u8 u8PinNCpy, const u8 u8StateCpy)
{
	volatile u8* portX;

	switch (enumPortCpy)
	{
		case port_A:
			portX = &PORT_A;
		break;

		case port_B:
			portX = &PORT_B;
		break;

		case port_C:
			portX = &PORT_C;
		break;

		case port_D:
			portX = &PORT_D;
		break;

		default:
		return;
	}

	BIT_ASSIGN(*portX, u8PinNCpy, u8StateCpy);
}

inline void DIO_vidToggle_portValue(DIO_port_t enumPortCpy)
{
    switch (enumPortCpy)
    {
        case port_A:
            PORT_A = ~PORT_A;
        break;

        case port_B:
            PORT_B = ~PORT_B;
        break;

        case port_C:
            PORT_C = ~PORT_C;
        break;

        case port_D:
            PORT_D = ~PORT_D;
        break;
    }
}

void DIO_vidToggle_pinValue(DIO_port_t enumPortCpy, const u8 u8PinNCpy)
{
    volatile u8* portX;

    switch (enumPortCpy)
    {
        case port_A:
            portX = &PORT_A;
        break;

        case port_B:
            portX = &PORT_B;
        break;

        case port_C:
            portX = &PORT_C;
        break;

        case port_D:
            portX = &PORT_D;
        break;

        default:
        return;
    }

    BIT_TOGGLE(*portX, u8PinNCpy);
}

inline void DIO_vidActivate_portPullUp(DIO_port_t enumPortCpy)
{
    DIO_vidSet_portValue(enumPortCpy, ON_ALL);
}

inline void DIO_vidDeactivate_portPullUp(DIO_port_t enumPortCpy)
{
    DIO_vidSet_portValue(enumPortCpy, OFF);
}

inline void DIO_vidActivate_pinPullUp(DIO_port_t enumPortCpy, const u8 u8PinNCpy)
{
	DIO_vidSet_pinValue(enumPortCpy, u8PinNCpy, ON);
}

inline void DIO_vidDeactivate_pinPullUp(DIO_port_t enumPortCpy, const u8 u8PinNCpy)
{
	DIO_vidSet_pinValue(enumPortCpy, u8PinNCpy, OFF);
}
// --------------- //

// --- getters --- //
inline u8 DIO_u8Get_portValue(DIO_port_t enumPortCpy)
{
	switch (enumPortCpy)
	{
		case port_A:
			return PIN_A;

		case port_B:
			return PIN_B;

		case port_C:
			return PIN_C;

		case port_D:
			return PIN_D;

		default:
			return 0;
	}
}

u8 DIO_u8Get_pinValue(DIO_port_t enumPortCpy, const u8 u8PinNCpy)
{
	volatile u8* pinX;

	switch (enumPortCpy)
	{
		case port_A:
			pinX = &PIN_A;
		break;

		case port_B:
			pinX = &PIN_B;
		break;

		case port_C:
			pinX = &PIN_C;
		break;

		case port_D:
			pinX = &PIN_D;
		break;

		default:
			return 0;
	}

	return BIT_GET(*pinX, u8PinNCpy);
}

/*
  arg1:       <DIO_port_t> required port
  arg2 (ret): <u8*> changed pin; [1 -> 8] if there's a change, 0 otherwise (timed-out and no change)
  arg3:       <u16> timeout value in ms
  ret:        <u8> port value after change or timeout
*/
u8 DIO_u8Wait_portInput(DIO_port_t enumPortCpy, u8* u8PtrChangedPinCpy, u16 u16Timeout_msCpy)
{
	volatile u8* pinX = 0; // 'volatile' to force-read addr whenever it's mentioned (see 'while' loop below)

	switch (enumPortCpy)
	{
		case port_A:
			pinX = &PIN_A;
		break;

		case port_B:
			pinX = &PIN_B;
		break;

		case port_C:
			pinX = &PIN_C;
		break;

		case port_D:
			pinX = &PIN_D;
		break;
	}

	u8 previousVal = *pinX;
	u8 changedPin_; // will be converted later to decimal value

	// trap until timeout or port-change
	while ( !(*pinX ^ previousVal) && u16Timeout_msCpy-- ) // while there's no difference and not timed-out
	{
		_delay_ms(1);
	}

	// we reach here if a change happened or we timed-out.

	// get changed pin, then current port value
	changedPin_ = *pinX ^ previousVal;
	previousVal = *pinX;

	// change binary value of 'changedPin_' to decimal
	*u8PtrChangedPinCpy = 0;

	while (changedPin_)
	{
		*u8PtrChangedPinCpy += 1;
		changedPin_ >>= 1;
	}

	return previousVal;
}

/*
  arg1:       <DIO_port_t> required port
  arg2:       <u8> required pin number (0 based)
  arg3 (ret): <u8*> 1 if required pin was changed, 0 if timed-out and no change
  arg4:       <u16> timeout value in ms
  ret:        <u8> pin value after change or timeout
*/
u8 DIO_u8Wait_pinInput(DIO_port_t enumPortCpy, const u8 u8PinNCpy, u8* isPinChangedCpy, u16 u16Timeout_msCpy)
{
	volatile u8* pinX = 0; // 'volatile' to force-read addr whenever it's mentioned (see 'while' loop below)

	switch (enumPortCpy)
	{
		case port_A:
			pinX = &PIN_A;
		break;

		case port_B:
			pinX = &PIN_B;
		break;

		case port_C:
			pinX = &PIN_C;
		break;

		case port_D:
			pinX = &PIN_D;
		break;
	}

	const u8 bitMask = 1 << u8PinNCpy;
	u8 previousVal = *pinX & bitMask;

	// trap until timeout or pin-change
	while ( !( (*pinX & bitMask) ^ previousVal ) && u16Timeout_msCpy-- ) // while there's no difference and not timed-out
	{
		_delay_ms(1);
	}

	// we reach here if a change happened or we timed-out.

	// get whether pin is changed or not
	*isPinChangedCpy = ((*pinX & bitMask) ^ previousVal) ? 1 : 0;

	return !!(*pinX & bitMask);
}
// --------------- //

