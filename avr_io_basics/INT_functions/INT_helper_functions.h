#ifndef AVR_IO_BASICS_INT_FUNCTIONS_INT_HELPER_FUNCTIONS_H_
#define AVR_IO_BASICS_INT_FUNCTIONS_INT_HELPER_FUNCTIONS_H_


#include "../../custom_types.h"

// external INT0-1 trigger type
typedef enum
{
	INT_trigger_Low_level    = 0b00000000,
	INT_trigger_Falling_edge = 0b00000010,
	INT_trigger_Rising_edge  = 0b00000011,
	INT_trigger_Any_chnage   = 0b00000001
} INT_trigger_t;

typedef void (*INT_CB_t)(void); // external interrupts callback type

void INT_vidEnable_global_flag(void);
void INT_vidDisable_global_flag(void);

void INT_vidEnable_external_INT(const u8 u8INTnumCpy);
void INT_vidDisable_external_INT(const u8 u8INTnumCpy);

void INT_vidSet_trigger_type(const u8 u8INTnumCpy, INT_trigger_t enumTriggerTypeCpy);

void INT_vidRegisterCB(const INT_CB_t CBfuncCpy, const u8 u8INTnumCpy);
void INT_vidDeregisterCB(const u8 u8INTnumCpy);


#endif /* AVR_IO_BASICS_INT_FUNCTIONS_INT_HELPER_FUNCTIONS_H_ */

