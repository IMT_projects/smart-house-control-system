#ifndef IO_ADDRS_H
#define IO_ADDRS_H


#include "../custom_types.h"

/* addrs are 'volatile' since they're shared-access,
 hence avoids the hustle of compiler optimization with loops like this:

  while (*PIN_X) // wait while it's not 0
  {}

 usually this would result in value caching, resulting in an assembly code like this:

 Assembly:
        mov eax, [PIN_X] // caching
  loop: test eax,eax
        jne loop

 C/C++:
  while (1) // wait while it's not 0
  {}
*/

// ------ DIO addrs ------ //
// ----------------------- //
// Data Direction register
// 0 -> input, 1 -> output
#define DDR_A (*(volatile u8*)0x3A)
#define DDR_B (*(volatile u8*)0x37)
#define DDR_C (*(volatile u8*)0x34)
#define DDR_D (*(volatile u8*)0x31)

// Port Value register,
// when pin is output: set output value
// when pin is input:  0-> deactivate pull-up resistor, 1-> activate pull-up resistor
#define PORT_A (*(volatile u8*)0x3B)
#define PORT_B (*(volatile u8*)0x38)
#define PORT_C (*(volatile u8*)0x35)
#define PORT_D (*(volatile u8*)0x32)

// Port Input register
// used to query value when pin in input
#define PIN_A (*(volatile u8*)0x39)
#define PIN_B (*(volatile u8*)0x36)
#define PIN_C (*(volatile u8*)0x33)
#define PIN_D (*(volatile u8*)0x30)
// ----------------------- //
// ----------------------- //


// ------ INT addrs ------ //
// ----------------------- //
// Status Register
// bit7 global INT enable
#define MCU_STATUS (*(volatile u8*)0x5F)
// General Interrupt Control Register
// bit7 INT1 enable
// bit6 INT0 enable
// bit5 INT2 enable
#define MCU_GENERAL_INT_CTRL (*(volatile u8*)0x5B)
// MCU Control Register
// bit0-1 INT0 trigger type
// bit2-3 INT1 trigger type
#define MCU_CTRL (*(volatile u8*)0x55)
// MCU Control and Status Register
// bit6: INT2 trigger, 1 -> rising edge, 0 -> falling edge
#define MCU_CTRL_STATUS (*(volatile u8*)0x54)

// General Interrupt Flag Register (read purposes)
// these bits are set when an interrupt occurs, they're cleared when ISR is executed, or by writing 1 to them.
// bit7 INT1 (is there an interrupt)
// bit6 INT0 (is there an interrupt)
// bit5 INT2 (is there an interrupt)
#define MCU_GENERAL_INT_FLAG (*(volatile u8*)0x5A)
// ----------------------- //
// ----------------------- //


// ------ ADC addrs ------ //
// ----------------------- //
// ADC Control and Status Register A,
// bit7 enable ADC
// bit6 start conversion, auto cleared when conversion is complete
// bit5 auto-trigger enable (ADC will start a conversion on a positive edge of the selected trigger signal.
//                           The trigger source is selected by setting the ADC Trigger Select bits, ADTS in SFIOR)
// bit4 becomes 1 when conversion is complete and data is available, auto cleared when ISR is executed, or clear it by writing 1
// bit3 interrupt enable flag, calls ISR when conversion is complete
// bi2-0 ADC pre-scalar factor
#define ADC_CTRL_STATUS (*(volatile u8*)0x26)
// ADC Multiplexer Selection Register
// bit7-6 ref. voltage
// bit5 adjust type (low or high)
// bit4-0 channel and gain selection
#define ADC_MUX (*(volatile u8*)0x27)
// ADC high and low bytes
#define ADC_VALUE_HBYTE (*(volatile u8*)0x25)
#define ADC_VALUE_LBYTE (*(volatile u8*)0x24)
#define ADC_VALUE       (*(volatile u16*)0x24)
// ----------------------- //
// ----------------------- //


// ------ Timer addrs ------ //
// ------------------------- //
// ******** Timer 0 ******** //
// Timer/Counter Control Register
// bit6,3 Waveform Generation Mode (Normal, CTC, PWM, PWM phase correct)
// bit5-4 output wave behavior when compare-match happens (normal, toggle, clear, set)
// bit2-0 prescaler and off mode
#define TIMER0_CTRL (*(volatile u8*)0x53)
// Timer/Counter value
// read or write the timer value
#define TIMER0_VALUE (*(volatile u8*)0x52)
// Output Compare Register
// holds the value that'll be compared against 'TIMER0_VALUE'
#define TIMER0_CMP (*(volatile u8*)0x5C)
// ************************* //

// ******** Timer 2 ******** //
// Timer/Counter Control Register
// bit6,3 Waveform Generation Mode (Normal, CTC, PWM, PWM phase correct)
// bit5-4 output wave behavior when compare-match happens (normal, toggle, clear, set)
// bit2-0 prescaler and off mode
#define TIMER2_CTRL (*(volatile u8*)0x45)
// Timer/Counter value
// read or write the timer value
#define TIMER2_VALUE (*(volatile u8*)0x44)
// Output Compare Register
// holds the value that'll be compared against 'TIMER0_VALUE'
#define TIMER2_CMP (*(volatile u8*)0x43)
// ************************* //

// ******** Timer 1 ******** //
// Timer/Counter1 Control Register A
// bit7-6 ch_A output wave behavior when compare-match happens (normal, toggle, clear, set)
// bit5-4 ch_B output wave behavior when compare-match happens (normal, toggle, clear, set)
// bit1-0 Waveform Generation Mode (Normal, CTC, PWM, PWM phase correct)
#define TIMER1A_CTRL (*(volatile u8*)0x4F)
// Timer/Counter1 Control Register B
// bit6 Input Capture trigger type, 0: falling edge, 1: rising edge
// bit4-3 Waveform Generation Mode (Normal, CTC, PWM, PWM phase correct)
// bit2-0 prescaler and off mode
#define TIMER1B_CTRL (*(volatile u8*)0x4E)
// Timer/Counter1 value
// read or write the timer value
#define TIMER1_VALUE_LBYTE (*(volatile u8*)0x4C)
#define TIMER1_VALUE_HBYTE (*(volatile u8*)0x4D)
#define TIMER1_VALUE       (*(volatile u16*)0x4C)
// Output Compare Register 1A
// holds the value that'll be compared against 'TIMER1_VALUE'
//#define TIMER1A_CMP_LBYTE (*(volatile u8*)0x4A)
//#define TIMER1A_CMP_HBYTE (*(volatile u8*)0x4B)
#define TIMER1A_CMP       (*(volatile u16*)0x4A)
// Output Compare Register 1B
// holds the value that'll be compared against 'TIMER1_VALUE'
//#define TIMER1B_CMP_LBYTE (*(volatile u8*)0x48)
//#define TIMER1B_CMP_HBYTE (*(volatile u8*)0x49)
#define TIMER1B_CMP       (*(volatile u16*)0x48)
// Input Capture value
// holds the value of 'TIMER1_VALUE' when an input capture is triggered
//#define TIMER1_INPUT_CAPTURE_VALUE_LBYTE (*(volatile u8*)0x46)
//#define TIMER1_INPUT_CAPTURE_VALUE_HBYTE (*(volatile u8*)0x47)
#define TIMER1_INPUT_CAPTURE_VALUE       (*(volatile u16*)0x46)
// ************************* //

// Timer/Counter Interrupt Mask Register
// bit0 Timer0 overflow interrupt enable
// bit1 Timer0 compare-match interrupt enable
// bit2 Timer1 overflow interrupt enable
// bit3 Timer1-B compare-match interrupt enable
// bit4 Timer1-A compare-match interrupt enable
// bit5 Timer1 Input Capture interrupt enable
// bit6 Timer2 overflow interrupt enable
// bit7 Timer2 compare-match interrupt enable
#define TIMER_INT_MASK (*(volatile u8*)0x59)
// Timer/Counter Interrupt Flag Register
// these bits are set when an interrupt occurs, they're cleared when ISR is executed, or by writing 1 to them.
// bit0 Timer0 overflow flag (is there overflow)
// bit1 Timer0 compare-match flag (is there a match)
// bit2 Timer1 overflow flag (is there overflow)
// bit3 Timer1-B compare-match flag (is there a match)
// bit4 Timer1-A compare-match flag (is there a match)
// bit5 Timer1 Input Capture flag (is Input Capture triggered)
// bit6 Timer2 overflow flag (is there overflow)
// bit7 Timer2 compare-match flag (is there a match)
#define TIMER_INT_STATUS (*(volatile u8*)0x58)
// ------------------------- //
// ------------------------- //


// ------ USART addrs ------ //
// ------------------------- //
// USART I/O Data Register
// holds value of Tx or Rx data
#define USART_RX_TX_DATA (*(volatile u8*)0x2C)
// USART Control and Status Register A
// bit7 (receive complete bit)
//      1: data is fully received in Rx buffer (all data received but not read),
//      0: Rx buffer is empty (or all data were read)
// bit6 (transmit complete bit)
//      1: data is fully transmitted from Tx buffer (all data transmitted),
//      0: Tx buffer is empty (or not all data transmitted)
// bit5 is Tx buffer ready to accept data ? (data register empty bit)
// bit4 is frame error ?
//      always clear this bit before writing to this register
// bit3 is data overrun ?
//      always clear this bit before writing to this register
// bit2 is parity error ?
//      always clear this bit before writing to this register
// bit1 is 2x transmission speed ? (only valid in asynch. mode)
// bit0 is  multi-processor communication mode ?
#define USART_CTRL_STATUS_A (*(volatile u8*)0x2B)
// USART Control and Status Register B
// bit7  Rx-complete interrupt enable
// bit6  Tx-complete interrupt enable
// bit5 data register empty interrupt enable
// bit4 Rx enable
// bit3 Tx enable
// bit2 last bit in character size selector
// bit1 Rx buffer 9th bit, must be read first before 'USART_RX_TX_DATA'
// bit0 Tx buffer 9th bit, must be written first before 'USART_RX_TX_DATA'
#define USART_CTRL_STATUS_B (*(volatile u8*)0x2A)
// USART Control and Status Register C, and Baud Rate high byte Register
// bit7 register select, 1: 'USART_CTRL_STATUS_C', 0: 'BAUD_RATE_HBYTE'
//      this bit must be written along with the required bits, not separately (one time switch)
// bit6 mode, 0:asynch, 1:synch
// bit5-4 parity mode (disabled, even, odd)
// bit3 stop bit select, 0: 1-bit, 1: 2-bits
// bit2-1 first 2 bits in character size selector
// bit0 clock polarity
#define USART_CTRL_STATUS_C_AND_BAUD_RATE_HBYTE (*(volatile u8*)0x40)
// USART Baud Rate low byte Register
// bit 11-0 baud rate
#define USART_BAUD_RATE_LBYTE (*(volatile u8*)0x29)
// ------------------------- //
// ------------------------- //


// ------ SPI addrs ------ //
// ----------------------- //
// SPI Control Register
// bit7 SPI interrupt enable
// bit6 SPI enable
// bit5 data order, 0 -> MSB is transmitted first, 1 -> LSB is transmitted first
// bit4 SPI mode, 0 -> slave, 1 -> master
// bit3 clock polarity, 0 -> SCK is low when idle, 1 -> SCK is high when idle
// bit2 clock phase (???)
// bit1-0 SCK frequency pre-scaler (for master, have no effect on the slave)
#define SPI_CTRL (*(volatile u8*)0x2D)
// SPI Status Register (for reading/polling purpose)
// bit7 interrupt flag, when serial transfer is complete this flag is set
//      bit is auto-cleared by ISR or by first reading the Status Register, then accessing the Data Register
// bit6 write collision flag, set if Data Register (SPDR) is written during a data transfer
//      cleared by first reading the Status Register, then accessing the Data Register
// bit5-1 reserved
// bit0 is double SCK frequency ?
#define SPI_STATUS (*(volatile u8*)0x2E)
// SPI Data Register
// Writing to the register initiates data transmission.
// Reading the register causes the Shift Register Receive buffer to be read.
#define SPI_DATA (*(volatile u8*)0x2F)
// ----------------------- //
// ----------------------- //


// ------ I2C addrs ------ //
// ----------------------- //
// TWI Bit Rate Register
// bit7-0 bit rate selector (see equation page 173)
#define I2C_BIT_RATE (*(volatile u8*)0x20)
// TWI Control Register
// bit7 Interrupt Flag (is action finished ?),
//      bit is set by hardware when TWI has finished its current action (Start, SLA+R/W, etc...).
//      Flag is cleared by writing 1 to it.
//      Flag is not automatically cleared by hardware when executing the ISR.
//      Also note that clearing this flag starts the operation/Action of the TWI,
//      hence all accesses to the Address Register, Status Register, and Data Register
//      must be complete/done before clearing this flag.
// bit6 Enable Acknowledge Bit, If this bit is set to one, the ACK pulse is generated on the TWI bus.
//      if bit cleared (set to zero), the device can be virtually disconnected from the bus temporarily,
//      address recognition can then be resumed by writing this bit to 1 again.
// bit5 START Condition, set to one to become a master on the bus,
//      this bit must be cleared by software when the START condition has been transmitted.
// bit4 STOP Condition, setting bit to 1 in ((Master)) mode will generate a STOP condition on the bus.
//           When the STOP condition is executed on the bus, this bit is cleared automatically.
//           In ((Slave)) mode, setting this bit to 1 can be used to recover from an error condition,
//           this will not generate a STOP condition, but the TWI returns to a well-defined un-addressed
//           slave-mode and releases the SCL and SDA lines to a high state.
// bit3 Write Collision Flag, bit is set when attempting to write to the Data Register (TWDR)
//      when bit7 is low (action is not finished, hence TWI is busy).
// bi2 TWI Enable, enables TWI operation and activates the TWI interface.
// bit0 Interrupt Enable, interrupt request will occur if bit7 is high (action is finished).
#define I2C_CTRL (*(volatile u8*)0x56)
// TWI Status Register
// bit7-3 Status (Action)
// bit1-0 Prescaler Bits
#define I2C_STATUS (*(volatile u8*)0x21)
// TWI Data
// It is writable while the TWI is not in the process of shifting a byte,
// this occurs when the TWI Interrupt Flag (I2C_CTRL.bit7) is set by hardware.
#define I2C_DATA (*(volatile u8*)0x23)
// Slave (and Master) Address Register
// bit7-1 address of slave
// bit0 is general-call respond ?
#define I2C_ADDRESS (*(volatile u8*)0x22)
// ----------------------- //
// ----------------------- //


// ------ EEPROMi addrs ------ //
// --------------------------- //
// The EEPROM Address Register
// bits15-10 reserved
// bits9-0 EEPROM address
#define EEPROMi_ADDRESS (*(volatile u16*)0x3E)
// EEPROM Data Register
// bits7-0 EEPROM Data
#define EEPROMi_DATA (*(volatile u8*)0x3D)
// EEPROM Control Register
#define EEPROMi_CTRL (*(volatile u8*)0x3C)
// --------------------------- //
// --------------------------- //


#endif // IO_ADDRS_H

