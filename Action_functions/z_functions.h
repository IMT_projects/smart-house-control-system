/*
 * z_functions.h
 *
 *  Created on: Jan 30, 2018
 *      Author: Ahmed Nagy
 */

#ifndef ACTION_FUNCTIONS_Z_FUNCTIONS_H_
#define ACTION_FUNCTIONS_Z_FUNCTIONS_H_


void Solenoid_vidOpen(void);

void Solenoid_vidClosed(void);

void Solenoid_vidToggel(void);

void BUZZ_vidCorrect(void);

void BUZZ_vidWorng(void);

void Light_ON(void);

void Light_OFF(void);

void Light_Toggle(void);

#endif /* ACTION_FUNCTIONS_Z_FUNCTIONS_H_ */

