/*
 * z_functions.c
 *
 *  Created on: Jan 25, 2018
 *      Author: Ahmed Nagy
 */
#include "../bit_manip.h"
#include "../custom_types.h"
#include "../avr_io_basics/DIO_functions/DIO_helper_functions.h"
#include "../Relay_functions/relay_helper_functions.h"
#include <util/delay.h>

typedef struct
{
	DIO_port_t portLED;
	DIO_port_t portBazzer;

	u8 pin_LED_Green;
	u8 pin_LED_RED;
	u8 pin_vcc;		    // Relay Pin
	u8 pin_NO;			// Relay Pin Open only for testing

} BUZZ_LED_obj_t;

typedef struct
{
	DIO_port_t	portSol;
	u8 			pin_Sol_vcc;
} Solenoid_port_t;

typedef struct
{
	DIO_port_t	portLight;
	u8 			pinLight;
} Light_port_t;

//buzz.portLED		= PORT_B;		// Buzzer and LEDs Relay
//buzz.portBazzer		= PORT_B;
//buzz.pin_LED_Green 	= 1;
//buzz.pin_LED_RED	= 2;
//buzz.pin_vcc		= 0;
//sol.portSol			= PORT_B;		// Solenoid Relay
//sol.pin_Sol_vcc		= 3;
BUZZ_LED_obj_t buzz = {port_B, port_B, 1, 2, 0};
Solenoid_port_t sol = {port_B, 3};
Light_port_t light = {port_B, 4};


/*
// init relay 1
	Relay_obj_t relay_1;
	relay_1.portRelay = port_D;
	relay_1.pinRelay = 5;
	relay_1.Relay_off_state = OFF; // OFF: 0v = off, ON: 5v = off

	Relay_vidInit(&relay_1);


	// init relay 2
	Relay_obj_t relay_2;
	relay_2.portRelay = port_D;
	relay_2.pinRelay = 3;
	relay_2.Relay_off_state = OFF; // OFF: 0v = off, ON: 5v = off

	Relay_vidInit(&relay_2);

	// make a difference between relay 1 & 2
	Relay_vidActivate(&relay_2);

*/

void Solenoid_vidOpen(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidSet_pinValue(sol.portSol, sol.pin_Sol_vcc, ON);
}

void Solenoid_vidClosed(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidSet_pinValue(sol.portSol, sol.pin_Sol_vcc, OFF);
}

void Solenoid_vidToggel(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidToggle_pinValue(sol.portSol, sol.pin_Sol_vcc);
}

void BUZZ_vidCorrect(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, ON);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_Green, ON);
	_delay_ms(200);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, OFF);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_Green, OFF);
	_delay_ms(200);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, ON);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_Green, ON);
	_delay_ms(200);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, OFF);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_Green, OFF);
}

void BUZZ_vidWorng(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, ON);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_RED, ON);
	_delay_ms(1000);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, OFF);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_RED, OFF);
	_delay_ms(500);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, ON);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_RED, ON);
	_delay_ms(1000);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_vcc, OFF);
	DIO_vidSet_pinValue(buzz.portBazzer, buzz.pin_LED_RED, OFF);
}

void Light_ON(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidSet_pinValue(light.portLight, light.pinLight, ON);
}

void Light_OFF(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidSet_pinValue(light.portLight, light.pinLight, OFF);
}

void Light_Toggle(void)
{
	DIO_vidSet_portDirection(port_B, OUTPUT);

	DIO_vidToggle_pinValue(light.portLight, light.pinLight);
}

